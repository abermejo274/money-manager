import React, { useState } from "react"
import { Container } from "react-bootstrap"
import { BrowserRouter as Router } from "react-router-dom"
import { Route, Switch } from "react-router-dom"
import { UserProvider } from "userContext"

//Pages
import Register from "pages/register"
import Login from "pages/login"
import Home from "pages/home"
import Categories from "pages/categories"
import Logout from "pages/logout"
import AddCategories from "pages/addCategories"
import Income from "pages/income"
import Expenses from "pages/expenses"
import AddExpenseEntries from "pages/addExpenseEntries"
import AddIncomeEntries from "pages/addIncomeEntries"
import AllEntries from "pages/allEntries"

//Components
import NavBar from "components/NavBar"

import './App.css';

function App() {

  const [user, setUser] = useState({
    email: localStorage.getItem("email"),
    isAdmin: localStorage.getItem("isAdmin") === "true"
  })
  // console.log(user)

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <NavBar />
        <Container>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/categories" component={Categories} />
            <Route exact path="/logout" component={Logout} />
            <Route exact path="/add-category" component={AddCategories} />
            <Route exact path="/income" component={Income} />
            <Route exact path="/expenses" component={Expenses} />
            <Route exact path="/add-expense" component={AddExpenseEntries} />
            <Route exact path="/add-income" component={AddIncomeEntries} />
            <Route exact path="/home" component={AllEntries} />
          </Switch>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;

