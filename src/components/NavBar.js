import React, { useContext } from "react"
import { Navbar, Nav, Image } from "react-bootstrap"
import { Link, NavLink } from "react-router-dom"
import Logo from "images/logo.png"

import UserContext from "userContext"

export default function NavBar() {

    const { user } = useContext(UserContext)

    return (
        <Navbar expand="lg">
            <Navbar.Brand as={Link} to="/">
                <Image src={Logo} />
                <span>
                    Money Manager
                </span>
            </Navbar.Brand>
            <Navbar.Toggle bg="dark" aria-controls="nav" />
            <Navbar.Collapse id="nav">
                {
                    user.email
                        ?
                        <>
                            <Nav.Link as={NavLink} to="/categories">Categories</Nav.Link>
                            <Nav.Link as={NavLink} to="/expenses">Expenses</Nav.Link>
                            <Nav.Link as={NavLink} to="/income">Income</Nav.Link>
                            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                        </>
                        :
                        <>
                            <Nav.Link as={NavLink} to="/login">
                                Log In
                            </Nav.Link>
                            <Nav.Link as={NavLink} to="/register">
                                Sign Up
                            </Nav.Link>
                        </>
                }
            </Navbar.Collapse>
        </Navbar>
    )
}