import React, { useState } from "react"
import { Col, Card, Button } from "react-bootstrap"
import { Redirect } from "react-router-dom"
import Swal from "sweetalert2"

export default function Category({ category }) {

    const { name, type, _id } = category
    // console.log(category)
    // console.log(category._id)

    const [willRedirect, setWillRedirect] = useState(false)

    function deleteCategory(e) {
        e.preventDefault()
        fetch(`https://boiling-peak-59004.herokuapp.com/api/categories/${_id}`, {
            method: "DELETE",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                "Content-Type": "application/json"
            }
        })
        Swal.fire({
            icon: "success",
            title: "Success",
            text: "Category Deleted"
        })
        setWillRedirect(true)
    }

    function updateCategory(e) {
        e.preventDefault()
        Swal.fire({
            title: 'Update Category',
            html: `<input type="text" id="updateCategory" class="swal2-input" placeholder="${name}">
        	<input type="text" id="updateType" class="swal2-input" placeholder="${type}">`,
            confirmButtonText: 'Update',
            allowOutsideClick: false,
            allowEscapeKey: false,
            preConfirm: () => {
                const updateType = Swal.getPopup().querySelector('#updateType').value
                const updateCategory = Swal.getPopup().querySelector('#updateCategory').value
                if (!updateCategory && !updateType) {
                    Swal.showValidationMessage(`Please input category and type`)
                }
                return { updateCategory, updateType }
            }
        }).then((data) => {
            console.log(data.value)

            fetch(`https://boiling-peak-59004.herokuapp.com/api/categories/${_id}`, {
                method: "PUT",
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    name: data.value.updateCategory,
                    type: data.value.updateType
                })
            })
            Swal.fire({
                icon: "success",
                title: "Success",
                text: "Category Updated"
            })
            setTimeout(() => {
                window.location.reload()
            }, 600)
            setWillRedirect(true)
        })
    }

    return (
        willRedirect
            ?
            <Redirect to="/categories" />
            :
            <Col xs={12} md={3}>
                <div id="category">
                    <Card className="mb-3">
                        <Card.Header>
                            <Card.Title>
                                {name}
                            </Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <Card.Text>
                                Type: <span>{type}</span>
                            </Card.Text>
                        </Card.Body>
                        <Card.Footer>
                            <Button variant="success" size="sm" onClick={updateCategory} className="mx-1">Update</Button>
                            <Button variant="danger" size="sm" onClick={deleteCategory} className="mx-1">Delete</Button>
                        </Card.Footer>
                    </Card>
                </div>
            </Col>
    )
}