import React from "react"
// import { Button } from "react-bootstrap"
// import { Redirect } from "react-router-dom"
// import Swal from "sweetalert2"

export default function ExpenseEntry({ entry }) {

    const { category, amount, _id } = entry
    // console.log(category)
    // console.log(category._id)

    // const [willRedirect, setWillRedirect] = useState(false)

    // function deleteEntry(e) {
    //     e.preventDefault()
    //     fetch(`https://boiling-peak-59004.herokuapp.com/api/entries/${_id}`, {
    //         method: "DELETE",
    //         headers: {
    //             Authorization: `Bearer ${localStorage.getItem('token')}`,
    //             "Content-Type": "application/json"
    //         }
    //     })
    //         .then(res => res.json())
    //         .then(data => {
    //             // console.log(data)
    //             if (data.message === "Entry has been deleted.") {
    //                 Swal.fire({
    //                     icon: "success",
    //                     title: "Success",
    //                     text: "Entry Deleted"
    //                 })
    //                 setWillRedirect(true)
    //             }
    //         })
    // }

    return (
        // willRedirect
        //     ?
        //     <Redirect to="/expenses" />
        //     :
        <tr key={_id}>
            <td>{category}</td>
            <td>{amount}</td>
            {/* <td>
                <Button value={_id} variant="danger">
                    onClick={deleteEntry}
                    Delete
                </Button>
            </td> */}
        </tr>
    )
}