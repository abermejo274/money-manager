import React, { useState, useEffect, useContext } from "react"
import { Row, Col, Form, Button } from "react-bootstrap"
import { Redirect } from "react-router-dom"
import UserContext from "userContext"

import Swal from "sweetalert2"

export default function AddCategories() {
    const [name, setName] = useState("")
    const [type, setType] = useState("")

    useEffect(() => {
        if (
            name !== "" && type !== ""
        ) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [name, type])

    //conditionally render our button
    const [isActive, setIsActive] = useState(false)

    //state for redirect
    const [willRedirect, setWillRedirect] = useState(false)

    //userContext
    const { user } = useContext(UserContext)
    // console.log(user)

    function addCategory(e) {
        e.preventDefault()

        fetch("https://boiling-peak-59004.herokuapp.com/api/categories/", {
            method: "POST",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                name: name,
                type: type
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                // console.log(data)
                if (data.error === "Category already exists.") {
                    Swal.fire({
                        icon: "error",
                        title: "Error",
                        text: data.error
                    })
                } else {
                    Swal.fire({
                        icon: "success",
                        title: "Success",
                        text: data.message
                    })

                    setWillRedirect(true)
                }
            })

        setName("")
        setType("")
    }

    return (
        user.email
            ?
            willRedirect
                ?
                < Redirect to="/categories" />
                :
                <>
                    <div id="addCategories">
                        <Row>
                            <Col id="formAddCategories" xs={12} md={6}>
                                <h3 className="text-center">Add Category</h3>
                                <Form onSubmit={e => addCategory(e)}>
                                    <Form.Group>
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control
                                            type="text"
                                            value={name}
                                            onChange={(e) => {
                                                setName(e.target.value)
                                            }}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Type</Form.Label>
                                        <Form.Control
                                            as="select"
                                            value={type}
                                            onChange={(e) => {
                                                setType(e.target.value)
                                            }}>
                                            <option hidden>Select Type</option>
                                            <option value="income">Income</option>
                                            <option value="expense">Expense</option>
                                        </Form.Control>
                                    </Form.Group>
                                    {
                                        isActive
                                            ?
                                            <div className="text-center">
                                                <Button variant="primary" type="submit">
                                                    Add Category
                                                </Button>
                                            </div>
                                            :
                                            <div className="text-center">
                                                <Button variant="primary" type="submit" disabled>
                                                    Add Category
                                                </Button>
                                            </div>
                                    }
                                </Form>
                            </Col>
                        </Row>
                    </div>
                </>
            :
            < Redirect to="/login" />
    )
}