import React, { useState, useEffect, useContext } from "react"
import { Button, Table } from "react-bootstrap"
import { Link, Redirect } from "react-router-dom"
import UserContext from "userContext"

import ExpenseEntry from "components/ExpenseEntry"

export default function Expenses() {

    const { user } = useContext(UserContext)

    const [expenseEntries, setExpenseEntries] = useState([])
    const [totalExpense, setTotalExpense] = useState(0)

    useEffect(() => {
        if (user.email) {
            fetch("https://boiling-peak-59004.herokuapp.com/api/entries/expenses", {
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem("token")}`
                }
            })
                .then(res => res.json())
                .then(data => {
                    // console.log(data)
                    setExpenseEntries(data.map(entry => {
                        return (
                            <ExpenseEntry key={entry._id} entry={entry} />
                        )
                    }))
                    // console.log(totalExpense)
                    let amountArr = data.map(element => {
                        return element.amount
                    })
                    if (amountArr.length >= 1) {
                        // console.log(amountArr)
                        let total = amountArr.reduce((x, y) => x + y)
                        // console.log(total)
                        setTotalExpense(total)
                    }
                })
        }
    }, [user])

    return (
        user.email
            ?
            <>
                <div id="expenses">
                    <h1 className="text-center my-3">Expenses</h1>
                    <div className="text-center my-3">
                        <Button as={Link} to="/add-expense">
                            Add Record
                        </Button>
                    </div>
                    <div className="text-center my-3">
                        <h5>Total Expenses: ₱{totalExpense}</h5>
                    </div>
                    <Table striped bordered hover className="text-center">
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Price (₱)</th>
                                {/* <th>Action</th> */}
                            </tr>
                        </thead>
                        <tbody>
                            {expenseEntries}
                        </tbody>
                    </Table>
                </div>
            </>
            :
            <Redirect to="/login" />
    )
}