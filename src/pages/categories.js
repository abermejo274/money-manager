import React, { useState, useEffect } from "react"
import { Row, Button } from "react-bootstrap"
import { Link } from "react-router-dom"

//component
import Category from "components/Category"

export default function Categories() {

    const [allCategories, setAllCategories] = useState([])

    useEffect(() => {
        fetch("https://boiling-peak-59004.herokuapp.com/api/categories/", {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
            .then(res => res.json())
            .then(data => {
                setAllCategories(data.map(category => {
                    return (
                        <Category key={category._id} category={category} />
                    )
                }))
            })
    }, [])
    // console.log(allCategories)

    return (
        <>
            <div id="categories">
                <h1 className="text-center my-3">Categories</h1>
                <div className="text-center my-3">
                    <Button as={Link} to="/add-category" className="btnCategoryPage">
                        Add Category
                    </Button>
                </div>
                <Row>
                    {allCategories}
                </Row>
            </div>
        </>

    )
}