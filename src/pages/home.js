import React, { useContext } from "react"
import { Link, Redirect } from "react-router-dom"
import { Row, Col, Jumbotron, Button } from "react-bootstrap"

import UserContext from "userContext"

export default function Home() {

    const { user } = useContext(UserContext)

    return (
        user.email
            ?
            < Redirect to="/home" />
            :
            <>
                <div id="main">
                    <Jumbotron>
                        <h1>Budgeting is the first step towards financial freedom</h1>
                        <Row>
                            <Col xs={12} md={8} >
                                <p>
                                    The Money Manager is an online money management tool that will help you to keep track of your income and expenses.
                                </p>
                            </Col>
                        </Row>
                        <div id="main-div-btn">
                            <Button id="main-btn" variant="warning" as={Link} to="/register">Let's Start</Button>
                        </div>
                    </Jumbotron>
                </div>
            </>
    )
}