import React, { useState, useEffect, useContext } from "react"
import { Button, Table } from "react-bootstrap"
import { Link, Redirect } from "react-router-dom"
import UserContext from "userContext"
import IncomeEntry from "components/IncomeEntry"

export default function Income() {

    const { user } = useContext(UserContext)

    const [incomeEntries, setIncomeEntries] = useState([])
    const [totalIncome, setTotalIncome] = useState(0)

    useEffect(() => {
        if (user.email) {
            fetch("https://boiling-peak-59004.herokuapp.com/api/entries/income", {
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem("token")}`
                }
            })
                .then(res => res.json())
                .then(data => {
                    // console.log(data)
                    setIncomeEntries(data.map(entry => {
                        return (
                            <IncomeEntry key={entry._id} entry={entry} />
                        )
                    }))
                    let amountArr = data.map(element => {
                        return element.amount
                    })
                    if (amountArr.length >= 1) {
                        // console.log(amountArr)
                        let total = amountArr.reduce((x, y) => x + y)
                        // console.log(total)
                        setTotalIncome(total)
                    }
                })
        }
    }, [user])

    return (
        user.email
            ?
            <>
                <div id="income">
                    <h1 className="text-center my-3">Income</h1>
                    <div className="text-center my-3">
                        <Button as={Link} to="/add-income">
                            Add Record
                        </Button>
                    </div>
                    <div className="text-center my-3">
                        <h5>Total Income: ₱{totalIncome}</h5>
                    </div>
                    <Table striped bordered hover className="text-center">
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Price (₱)</th>
                                {/* <th>Action</th> */}
                            </tr>
                        </thead>
                        <tbody>
                            {incomeEntries}
                        </tbody>
                    </Table>
                </div>
            </>
            :
            <Redirect to="/login" />
    )
}