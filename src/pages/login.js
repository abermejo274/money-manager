import React, { useState, useEffect, useContext } from "react"
import { Row, Col, Form, Button } from "react-bootstrap"
import { Redirect } from "react-router-dom"
import Swal from "sweetalert2"
import UserContext from "userContext"

export default function Login() {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    //conditionally render the submit button
    const [isActive, setIsActive] = useState(false)

    //state for redirect
    const [willRedirect, setWillRedirect] = useState(false)

    //useContext
    const { user, setUser } = useContext(UserContext)

    useEffect(() => {
        if (email !== "" && password !== "") {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    function loginUser(e) {
        e.preventDefault()

        fetch("https://boiling-peak-59004.herokuapp.com/api/users/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then((res) => res.json())
            .then((data) => {
                // console.log(data)
                if (data.accessToken) {
                    localStorage.setItem('token', data.accessToken)
                    Swal.fire({
                        icon: "success",
                        title: "Success",
                        text: "Thank you for logging to Money Manager"
                    })
                    //get user details from token
                    fetch('https://boiling-peak-59004.herokuapp.com/api/users/', {
                        headers: {
                            Authorization: `Bearer ${data.accessToken}`
                        }
                    })
                        .then(res => res.json())
                        .then(data => {
                            localStorage.setItem('email', data.email)
                            localStorage.setItem('isAdmin', data.isAdmin)

                            setWillRedirect(true)

                            setUser({
                                email: data.email,
                                isAdmin: data.isAdmin
                            })
                        })
                } else if (data.err === "Email is not registered.") {
                    Swal.fire({
                        icon: "error",
                        title: "Error",
                        text: "Couldn't find user. Please register first."
                    })
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Error",
                        text: "The password you entered is incorrect. Please try again."
                    })
                }
            })
        //clear out the states to their initial values
        setEmail("")
        setPassword("")
    }

    return (
        user.email
            ?
            <Redirect to="/home" />
            :
            willRedirect
                ?
                <Redirect to="/home" />
                :
                <>
                    <div id="login">
                        <Row>
                            <Col id="form-login" xs={12} md={6}>
                                <h1 className="text-center my-3">User Login</h1>
                                <Form onSubmit={e => loginUser(e)}>
                                    <Form.Group>
                                        <Form.Label>
                                            Email
                                        </Form.Label>
                                        <Form.Control
                                            type="email"
                                            placeholder="Enter Email"
                                            value={email}
                                            onChange={e => {
                                                setEmail(e.target.value)
                                            }}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>
                                            Password
                                        </Form.Label>
                                        <Form.Control
                                            type="password"
                                            placeholder="Enter Password"
                                            value={password}
                                            onChange={e => {
                                                setPassword(e.target.value)
                                            }}
                                            required
                                        />
                                    </Form.Group>
                                    {
                                        isActive
                                            ?
                                            <div className="text-center">
                                                <Button variant="primary" type="submit">
                                                    Log In
                                                </Button>
                                            </div>
                                            :
                                            <div className="text-center">
                                                <Button variant="primary" type="submit" disabled>
                                                    Log In
                                                </Button>
                                            </div>
                                    }
                                </Form>
                            </Col>
                        </Row>
                    </div>
                </>
    )
}