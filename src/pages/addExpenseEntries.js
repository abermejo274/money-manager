import React, { useState, useEffect, useContext } from "react"
import { Col, Row, Form, Button } from "react-bootstrap"
import { Redirect } from "react-router-dom"
import UserContext from "userContext"

import Swal from "sweetalert2"

export default function AddExpenseEntries() {

    const { user } = useContext(UserContext)

    const [category, setCategory] = useState("")
    const [amount, setAmount] = useState("")
    const [type, setType] = useState("")
    const [expenseCategories, setExpenseCategories] = useState([])

    useEffect(() => {
        if (
            category !== "" && amount !== ""
        ) {
            setIsActive(true)
            setType("expense")
        } else {
            setIsActive(false)
        }
    }, [category, amount])

    useEffect(() => {
        if (user.email) {
            fetch("https://boiling-peak-59004.herokuapp.com/api/categories/", {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then((res) => res.json())
                .then((data) => {
                    // console.log(data)
                    let expenseTypeCategory = data.filter(category => {
                        return category.type === "expense"
                    })
                    // console.log(expenseTypeCategory)
                    setExpenseCategories(expenseTypeCategory.map(category => {
                        return (
                            <option key={category._id}>{category.name}</option>
                        )
                    }))
                })
        }
    }, [user])

    //conditionally render our button
    const [isActive, setIsActive] = useState(false)

    //state for redirect
    const [willRedirect, setWillRedirect] = useState(false)

    function addExpenseEntry(e) {
        e.preventDefault()

        fetch("https://boiling-peak-59004.herokuapp.com/api/entries/", {
            method: "POST",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                category: category,
                amount: amount,
                type: type
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                // console.log(data)
                if (data.message === "Entry has been added.") {
                    Swal.fire({
                        icon: "success",
                        title: "Success",
                        text: data.message
                    })

                    setWillRedirect(true)
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Error"
                    })
                }
            })

        setCategory("")
        setAmount("")
        setType("")
    }

    return (
        user.email
            ?
            willRedirect
                ?
                <Redirect to="/expenses" />
                :
                <>
                    <div id="addExpense">
                        <Row>
                            <Col id="formAddExpense" xs={12} md={6}>
                                <h3 className="text-center my-3">Add Expense Record</h3>
                                <Form onSubmit={e => addExpenseEntry(e)}>
                                    <Form.Group>
                                        <Form.Label>Category</Form.Label>
                                        <Form.Control
                                            as="select"
                                            value={category}
                                            onChange={(e) => {
                                                setCategory(e.target.value)
                                            }}>
                                            <option hidden>Select Category</option>
                                            {expenseCategories}
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>Amount</Form.Label>
                                        <Form.Control
                                            type="number"
                                            min="0"
                                            max="999999999"
                                            value={amount}
                                            onChange={(e) => {
                                                setAmount(e.target.value)
                                            }}
                                            required
                                        />
                                    </Form.Group>
                                    {
                                        isActive
                                            ?
                                            <div className="text-center">
                                                <Button variant="primary" type="submit">
                                                    Add Record
                                                </Button>
                                            </div>
                                            :
                                            <div className="text-center">
                                                <Button variant="primary" type="submit" disabled>
                                                    Add Record
                                                </Button>
                                            </div>
                                    }
                                </Form>
                            </Col>
                        </Row>
                    </div>
                </>
            :
            <Redirect to="/login" />
    )
}