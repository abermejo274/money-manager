import React, { useState, useEffect, useContext } from "react"
import { Button, Table } from "react-bootstrap"
import { Link, Redirect } from "react-router-dom"

import UserContext from "userContext"

export default function AllEntries() {

    const { user } = useContext(UserContext)
    // console.log(user)

    const [allEntries, setAllEntries] = useState([])
    const [totalExpense, setTotalExpense] = useState(0)
    const [totalIncome, setTotalIncome] = useState(0)
    const [balance, setBalance] = useState(0)

    useEffect(() => {
        if (user.email) {
            fetch("https://boiling-peak-59004.herokuapp.com/api/entries/", {
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem("token")}`
                }
            })
                .then(res => res.json())
                .then(data => {
                    // console.log(data)
                    setAllEntries(data.map(entry => {
                        return (
                            <tr key={entry._id}>
                                <td>{entry.category}</td>
                                <td>{entry.amount}</td>
                                <td>{entry.type}</td>
                            </tr>
                        )
                    }))
                })
            fetch("https://boiling-peak-59004.herokuapp.com/api/entries/income", {
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem("token")}`
                }
            })
                .then(res => res.json())
                .then(data => {
                    let amountArr = data.map(element => {
                        return element.amount
                    })
                    if (amountArr.length >= 1) {
                        // console.log(amountArr)
                        let total = amountArr.reduce((x, y) => x + y)
                        // console.log(total)
                        setTotalIncome(total)
                    }
                })
            fetch("https://boiling-peak-59004.herokuapp.com/api/entries/expenses", {
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem("token")}`
                }
            })
                .then(res => res.json())
                .then(data => {
                    let amountArr = data.map(element => {
                        return element.amount
                    })
                    if (amountArr.length >= 1) {
                        // console.log(amountArr)
                        let total = amountArr.reduce((x, y) => x + y)
                        // console.log(total)
                        setTotalExpense(total)
                    }
                })
        }
    }, [user])

    useEffect(() => {
        setBalance(totalIncome - totalExpense)
    }, [totalExpense, totalIncome])

    return (
        (localStorage.getItem("email"))
            ?
            <>
                <div id="allEntries">
                    <h1 className="text-center my-3">All Transactions</h1>
                    <div className="text-center my-3">
                        <Button as={Link} to="/add-expense" className="m-3">
                            Add Expense Record
                        </Button>
                        <Button as={Link} to="/add-income" className="m-3">
                            Add Income Record
                        </Button>
                    </div>
                    <div className="text-center my-3">
                        <h5>Total Income: ₱{totalIncome}</h5>
                        <h5>Total Expenses: ₱{totalExpense}</h5>
                        <h5>Balance: ₱{balance}</h5>
                    </div>
                    <Table striped bordered hover className="text-center">
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Price (₱)</th>
                                <th>Type</th>
                            </tr>
                        </thead>
                        <tbody>
                            {allEntries}
                        </tbody>
                    </Table>
                </div>
            </>
            :
            <Redirect to="/login" />
    )
}