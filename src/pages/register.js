import React, { useState, useEffect, useContext } from "react"
import { Row, Col, Form, Button } from "react-bootstrap"
import { Redirect } from "react-router-dom"
import Swal from "sweetalert2"
import UserContext from "userContext"

export default function Register() {

    const { user } = useContext(UserContext)

    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [confirmPassword, setConfirmPassword] = useState("")

    //conditionally render the submit button
    const [isActive, setIsActive] = useState(false)

    //state for redirect
    const [willRedirect, setWillRedirect] = useState(false)

    useEffect(() => {
        if (
            firstName !== "" &&
            lastName !== "" &&
            email !== "" &&
            password !== "" &&
            confirmPassword !== "" &&
            password === confirmPassword
        ) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [firstName, lastName, email, password, confirmPassword])

    function registerUser(e) {
        e.preventDefault()

        fetch("https://boiling-peak-59004.herokuapp.com/api/users/", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password
            })
        })
            .then((res) => res.json())
            .then((data) => {
                // console.log(data)
                if (data.err === "Password is too short.") {
                    Swal.fire({
                        icon: "error",
                        title: "Error",
                        text: "Password must be at least 8 characters long."
                    })
                } else if (data.error === "Email already exists.") {
                    Swal.fire({
                        icon: "error",
                        title: "Error",
                        text: "Email already exists."
                    })
                } else {
                    Swal.fire({
                        icon: "success",
                        title: "Success",
                        text: "Your account has been created.",
                    })

                    setWillRedirect(true)
                }
            })
        //clear out the states to their initial values
        setFirstName("")
        setLastName("")
        setEmail("")
        setPassword("")
        setConfirmPassword("")
    }

    return (
        user.email
            ?
            <Redirect to="/home" />
            :
            willRedirect
                ?
                <Redirect to="/login" />
                :
                <>
                    <div id="register">
                        <Row>
                            <Col id="form-register" xs={12} md={6}>
                                <h1 className="text-center my-3">User Registration</h1>
                                <Form onSubmit={e => registerUser(e)}>
                                    <Form.Group>
                                        <Form.Label>
                                            First Name
                                        </Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Enter First Name"
                                            value={firstName}
                                            onChange={e => {
                                                setFirstName(e.target.value)
                                            }}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>
                                            Last Name
                                        </Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Enter Last Name"
                                            value={lastName}
                                            onChange={e => {
                                                setLastName(e.target.value)
                                            }}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>
                                            Email
                                        </Form.Label>
                                        <Form.Control
                                            type="email"
                                            placeholder="Enter Email"
                                            value={email}
                                            onChange={e => {
                                                setEmail(e.target.value)
                                            }}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>
                                            Password
                                        </Form.Label>
                                        <Form.Control
                                            type="password"
                                            placeholder="Enter Password"
                                            value={password}
                                            onChange={e => {
                                                setPassword(e.target.value)
                                            }}
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label>
                                            Confirm Password
                                        </Form.Label>
                                        <Form.Control
                                            type="password"
                                            placeholder="Confirm Password"
                                            value={confirmPassword}
                                            onChange={e => {
                                                setConfirmPassword(e.target.value)
                                            }}
                                            required
                                        />
                                    </Form.Group>
                                    {
                                        isActive
                                            ?
                                            <div className="text-center">
                                                <Button variant="primary" type="submit">
                                                    Sign Up
                                                </Button>
                                            </div>
                                            :
                                            <div className="text-center">
                                                <Button variant="primary" type="submit" disabled>
                                                    Sign Up
                                                </Button>
                                            </div>
                                    }
                                </Form>
                            </Col>
                        </Row>
                    </div>
                </>
    )
}