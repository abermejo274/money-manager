import React, { useContext } from "react"
import { Redirect } from "react-router-dom"
import UserContext from "userContext"

export default function Logout() {

    const { setUser, unsetUser } = useContext(UserContext)

    //clear localStorage
    unsetUser()

    //set global user to null
    setUser({
        email: null,
        isAdmin: null
    })

    return (
        <Redirect to="/" />
    )
}